import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;


public class Pass extends JFrame implements ActionListener{

	//クラス変数　コンストラクタが呼び出される前に呼ばれる
	JTextArea text_area = new JTextArea();
	JButton button = new JButton("入力完了");


	//Passのコンストラクタ
	public Pass() {
		setTitle("PASS");
		setSize(400,500);

		this.text_area.setEditable(false);
		this.text_area.setText(randon());
		this.text_area.setPreferredSize(new Dimension(100, 100));
		this.button.setPreferredSize(new Dimension(200, 100));

		setLayout(new BorderLayout());//めいいっぱい広げる
		add(this.text_area,BorderLayout.NORTH);
		add(this.button,BorderLayout.SOUTH);//

		this.button.addActionListener(this);// アクションリスナーをボタンに登録する。
	}

	//ランダムな数字を生成しString型で出力
    public String randon() {
        Random rand = new Random();
        String s = String.valueOf(rand.nextInt(9));
        return s;
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		String text = this.text_area.getText();
		text = text + "ボタンが押されました。\n";
		this.text_area.setText(text);
	}

}
