import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public class Coupon extends JFrame {

    public Coupon() throws HeadlessException {
        final JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(900, 900));

        ImageIcon icon1 = new ImageIcon("C:\\Users\\ctctime24\\Desktop\\cafe.jpg");
        ImageIcon icon2 = new ImageIcon("C:\\Users\\ctctime24\\Desktop\\restaurant.jpg");
        ImageIcon icon3 = new ImageIcon("C:\\Users\\ctctime24\\Desktop\\cinema.jpg");
        ImageIcon icon4 = new ImageIcon("C:\\Users\\ctctime24\\Desktop\\karaoke.jpg");
        ImageIcon icon5 = new ImageIcon("C:\\Users\\ctctime24\\Desktop\\yakiniku.jpg");

        JLabel label1 = new JLabel(icon1);
        JLabel label2 = new JLabel(icon2);
        JLabel label3 = new JLabel(icon3);
        JLabel label4 = new JLabel(icon4);
        JLabel label5 = new JLabel(icon5);

        label1.setIcon(icon1);
        label1.setText("喫茶店");
        label1.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 20));
        label1.setHorizontalAlignment(JLabel.LEFT);
        label2.setIcon(icon2);
        label2.setText("レストラン");
        label2.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 20));
        label2.setHorizontalAlignment(JLabel.LEFT);
        label3.setIcon(icon3);
        label3.setText("映画館");
        label3.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 20));
        label3.setHorizontalAlignment(JLabel.LEFT);
        label4.setIcon(icon4);
        label4.setText("カラオケ");
        label4.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 20));
        label4.setHorizontalAlignment(JLabel.LEFT);
        label5.setIcon(icon5);
        label5.setText("焼肉屋");
        label5.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 20));
        label5.setHorizontalAlignment(JLabel.LEFT);


        panel.add(label1);
        panel.add(label2);
        panel.add(label3);
        panel.add(label4);
        panel.add(label5);

        final JScrollPane scroll = new JScrollPane(panel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        add(scroll, BorderLayout.CENTER);
        setSize(300, 300);
        setVisible(true);
    }

    public static void main(final String[] args) throws Exception {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Coupon().setVisible(true);
            }
        });
    }
}