import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Top extends JPanel
{

	JButton button1 = new JButton("Reserve");
	JButton button2 = new JButton("予約");
	JButton button3 = new JButton("返却");
	MainFrame mf;
	String str;
	public Top(MainFrame mf ,String s)
	{
		this.mf = mf;
		str = s;
		this.setSize(400,400);
		button1.setPreferredSize(new Dimension(200, 100));
		button2.setPreferredSize(new Dimension(200,100));
		button3.setPreferredSize(new Dimension(300, 100));
		
		button1.setFont(new Font("Arial BOLD ITALIC", Font.ITALIC, 15));
		button2.setFont(new Font("HGS創英角ポップ体", Font.BOLD, 15));
		button3.setFont(new Font("HGS創英角ポップ体", Font.BOLD, 15));
		
		JLabel label1 = new JLabel("Umbrella                    ");
		JLabel label2 = new JLabel("モバイルバッテリー");
		
		label1.setFont(new Font("Arial BOLD ITALIC", Font.BOLD, 15));
		label2.setFont(new Font("HGS創英角ポップ体", Font.BOLD, 15));
		
	
		add(label1);
		add(button1, BorderLayout.NORTH);
		button1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				pc(mf.PanelNames[1]);
			}
		});
		add(label2);
		add(button2, BorderLayout.CENTER);
		button2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				pc(mf.PanelNames[1]);
			}
		});
		add(button3, BorderLayout.SOUTH);
		button3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				pc(mf.PanelNames[1]);
			}
		});

	}
	public void pc(String str) {
		
		mf.PanelChange(str);
		
	}
	}
