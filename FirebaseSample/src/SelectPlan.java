import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class SelectPlan implements ActionListener{
	  JTextArea text_area = new JTextArea();

  public static void main(String[] args) {

    JFrame frame = new JFrame("プランの選択");
 //   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(600, 400);
 //   frame.setLocationRelativeTo(null);

    // コンポーネントの配置方法を設定
    frame.setLayout(new FlowLayout());

    // ボタン1を作成
    JButton button1 = new JButton("1 Hourプラン");
    button1.addActionListener(e -> {
      System.out.println("ボタン1が押された");
    });

    // ボタン2を作成
    JButton button2 = new JButton("24 Hourプラン");
    button2.addActionListener(e -> {
      System.out.println("ボタン2が押された");
    });

    // 各ボタンをウィンドウに追加
    frame.add(button1);
    frame.add(button2);

    frame.setVisible(true);
  }
  public void actionPerformed(ActionEvent e)
  {
    String text = text_area.getText();
    text = text + "ボタンが押されました。\n";
    text_area.setText(text);
  }
}