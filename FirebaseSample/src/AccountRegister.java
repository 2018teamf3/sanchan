import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/*
 * フローレイアウトで強制改行するサンプルです。
 * ・やってることは、JSeparatorを縦幅0、横幅ウィンドウと同じサイズで追加するだけです。
 */
public class AccountRegister extends JFrame implements ActionListener{
	  static JTextField text1;
	  static JTextField text2;
	  static JTextField text3;
	  static JTextField text4;
	  static String method;

	  static JLabel label;
	  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\test-67cc6-60b70b74957b.json"; // 認証情報の JSONファイル名のパスを指定。
	  static String URL = "https://test-67cc6.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。

	  public static void main(String[] args) throws Exception {
		new AccountRegister();

	    System.out.println("mainメソッドの処理が開始されました。");

	    // 初期化。

	    FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

	    FirebaseOptions options = new FirebaseOptions.Builder()
	        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
	        .setDatabaseUrl(URL)
	        .build();

	    FirebaseApp.initializeApp(options);


	    try
	    {
	      Thread.sleep(5000);
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    System.out.println("method="+method);
	    System.out.println("mainメソッドの処理が終了しました。");

	}

	public AccountRegister() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		int width = 400;
		setBounds(200, 100, width, 300);
		setTitle("会員登録");

	    text1 = new JTextField("", 20);
	    text2 = new JTextField("", 20);
	    text3 = new JTextField("", 20);
	    text4 = new JTextField("", 20);
	    JRadioButton rad1 = new JRadioButton("クレジットカード");
	    JRadioButton rad2 = new JRadioButton("携帯キャリア");

	    rad1.addActionListener(new CheckActionListener());
	    rad1.setName("クレジットカード");
	    rad2.addActionListener(new CheckActionListener());
	    rad2.setName("携帯キャリア");

	    ButtonGroup group = new ButtonGroup();
	    group.add(rad1);
	    group.add(rad2);


		// フローレイアウト設定
		FlowLayout l = new FlowLayout(FlowLayout.LEFT);
		l.setVgap(3);
		l.setHgap(5);
		setLayout(l);
		setResizable(false); // サイズ変更禁止

		add(getHr(width, 0));
		add(getLabel("お名前"));
		add(text1);

		add(getHr(width, 0));
		add(getLabel("ふりがな"));
		add(text2);

		add(getHr(width, 0));
		add(getLabel("メールアドレス"));
		add(text3);

		add(getHr(width, 0));
		add(getLabel("連絡先"));
		add(text4);

		add(getHr(width, 0));
		add(getLabel("決済方式"));
		add(rad1);
		add(rad2);

		add(getHr(width, 0));
		add(getHr(width - 18, 1));

		// ボタンをセンタリング表示のためにパネルを追加
		JPanel p = new JPanel();
		p.setLayout(new FlowLayout());
		p.setPreferredSize(new Dimension(width - 18, 50));

	    JButton button = new JButton("登録します");
	    button.addActionListener(this);
	    label = new JLabel();
	    p.add(button);


	    Container contentPane = getContentPane();
	    contentPane.add(p, BorderLayout.CENTER);
	    contentPane.add(label, BorderLayout.SOUTH);
		setVisible(true);
	}

	  public void actionPerformed(ActionEvent e){
	    	sampleSendingDataToFirebase1();     // Firebase にデータを送信するサンプル1
		  }

	  public static void sampleSendingDataToFirebase1()
	  {
	    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    	reference.child("UserInformation").child("name").setValue(text1.getText());
    	reference.child("UserInformation").child("kana").setValue(text2.getText());
    	reference.child("UserInformation").child("Email").setValue(text3.getText());
    	reference.child("UserInformation").child("phoneNumber").setValue(text4.getText());
    	reference.child("UserInformation").child("transaction").setValue(method);

	    reference.child("time").push().setValue("送信時刻 " + new Date());
	  }

	public JLabel getLabel(String text) {
		JLabel label = new JLabel(text);
		label.setPreferredSize(new Dimension(120, 20));
		label.setHorizontalAlignment(JLabel.RIGHT);
		return label;
	}

	public JSeparator getHr(int width, int hight) {
		JSeparator sp = new JSeparator(JSeparator.HORIZONTAL);
		sp.setPreferredSize(new Dimension(width, hight));
		return sp;
	}
	class CheckActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JRadioButton s = (JRadioButton) e.getSource();
			if (s.isSelected()==true) {
				method=s.getName();
			}
//			System.out.println("Selected: " + s.isSelected());
//			System.out.println("Name: "+ s.getName());
		}
	}
}