import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.util.Date;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

class UnlockOrder extends JFrame{
    static int WIDTH  = 300;
    static int HEIGHT = 300;

    static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\open-key-system-firebase-adminsdk-a62ve-25a2fc9eff.json"; // 認証情報の JSONファイル名のパスを指定。
    static String URL = "https://open-key-system.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。

//    JLabel jLabel = new JLabel("countdown");
    JButton button = new JButton("開錠");

    static int count = 0;

    Timer timer = new java.util.Timer();



  public static void main(String args[])throws Exception{
    UnlockOrder frame = new UnlockOrder("開錠");
    frame.setVisible(true);
    System.out.println("mainメソッドの処理が開始されました。");

    // 初期化。

    FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

    FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl(URL)
        .build();

    FirebaseApp.initializeApp(options);

//	sampleSendingDataToFirebase1();     // Firebase にデータを送信するサンプル1

	   try
	    {
	      Thread.sleep(3000);
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }

	    System.out.println("mainメソッドの処理が終了しました。");
  }

  public static void sampleSendingDataToFirebase1()
  {
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    reference.child("openOrder").setValue(1);

    reference.child("time").push().setValue("送信時刻 " + new Date());
  }

  private void setLabel() {
//      add(jLabel);

      count = 30;
//      jLabel.setText(Integer.toString(count));
      button.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e) {

        		sampleSendingDataToFirebase1();
          }
        });
  }

  UnlockOrder(String title){
    setTitle(title);
    setBounds(100, 100, 300, 250);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JPanel p = new JPanel();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    setSize(WIDTH, HEIGHT);;

    setLabel();

    setVisible(true);
    button.setText("開錠");
    p.add(button);

    Container contentPane = getContentPane();
    contentPane.add(p, BorderLayout.CENTER);
  }
}