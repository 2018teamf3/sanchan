import javax.swing.JFrame;
//がんばりましょう卍卍
public class MainFrame extends JFrame{

	public String[] PanelNames = {"top","where","design"};
    Top top = new Top(this,this.PanelNames[0]);
    Where where = new Where(this,this.PanelNames[1]);
    Design design = new Design(this,this.PanelNames[2]);

    public MainFrame(){
        this.add(top);
        this.setBounds(100, 100, 400, 400);
    }
    public static void main(String[] args) {
        MainFrame mf = new MainFrame();
        mf.setDefaultCloseOperation(EXIT_ON_CLOSE);
        mf.setVisible(true);
    }
    public void PanelChange(String str){
    	if(str == PanelNames[1]) {
    		//this.remove(top);
    		//getContentPane().removeAll();
    		top.setVisible(false);
    		this.add(where);
    	}
    	else if(str == PanelNames[2]) {
    		where.setVisible(false);
    		this.add(design);
    	}
    }
}