import java.io.FileInputStream;
import java.util.Date;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.tasks.OnCompleteListener;
import com.google.firebase.tasks.Task;

public class FirebaseSample
{
  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\test-67cc6-60b70b74957b.json"; // 認証情報の JSONファイル名のパスを指定。
  static String URL = "https://test-67cc6.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。

  public static void main(String[] args) throws Exception
  {
    System.out.println("mainメソッドの処理が開始されました。");

    // 初期化。

    FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

    FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl(URL)
        .build();

    FirebaseApp.initializeApp(options);

    // Firebase へのデータ送受信を行うサンプル

    	sampleSendingDataToFirebase1();     // Firebase にデータを送信するサンプル1
 //   sampleSendingDataToFirebase2();     // Firebase にデータを送信するサンプル2
 //   sampleReceivingDataFromFirebase1(); // Firebase からデータを受信するサンプル1
 //   sampleReceivingDataFromFirebase2(); // Firebase からデータを受信するサンプル2

    // 上記のデータの送受信が終わるまで、しばらく時間がかかりますので、10秒間待機します。

    try
    {
      Thread.sleep(5000);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    System.out.println("mainメソッドの処理が終了しました。");
  }

  // Firebase にデータを送信するサンプル1

  public static void sampleSendingDataToFirebase1()
  {
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    reference.child("key1").setValue("139.701693");
    reference.child("key2").setValue("35.657992");
    reference.child("key3").setValue("渋谷駅");
    reference.child("key4").setValue("2");
    reference.child("key5").setValue("200");

    reference.child("time").push().setValue("送信時刻 " + new Date());
  }

  // Firebase にデータを送信するサンプル2

  public static void sampleSendingDataToFirebase2()
  {
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    OnCompleteListener listener = new OnCompleteListener()
    {
      @Override
      public void onComplete(Task task)
      {
        System.out.println("送信完了");

      }
    };

    reference.child("key1").setValue("こんにちは！").addOnCompleteListener(listener);
    reference.child("key2").setValue("ファイアーベース").addOnCompleteListener(listener);
    reference.child("key3").setValue("よろしくお願いします！").addOnCompleteListener(listener);

    reference.child("folder").child("a").setValue("あああ").addOnCompleteListener(listener);
    reference.child("folder").child("b").setValue("いいい").addOnCompleteListener(listener);
    reference.child("folder").child("c").setValue("ううう").addOnCompleteListener(listener);

    reference.child("time").push().setValue("送信時刻 " + new Date()).addOnCompleteListener(listener);
  }

  // Firebase からデータを受信するサンプル1

  public static void sampleReceivingDataFromFirebase1()
  {
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    ValueEventListener listener = new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot snapshot)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();
        System.out.println("データを受信しました。key=" + key + ", value=" + value);
      }

      @Override
      public void onCancelled(DatabaseError error)
      {
        System.out.println("キャンセルされました。");
      }
    };

    reference.child("key1").addValueEventListener(listener);
    reference.child("key2").addValueEventListener(listener);
    reference.child("key3").addValueEventListener(listener);
  }

  // Firebase からデータを受信するサンプル2

  public static void sampleReceivingDataFromFirebase2()
  {
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    ChildEventListener listener = new ChildEventListener()
    {
      @Override
      public void onChildAdded(DataSnapshot snapshot, String previousChildName)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();
        System.out.println("子供が追加されました。key=" + key + ", value=" + value);
        }

      @Override
      public void onChildChanged(DataSnapshot snapshot, String previousChildName)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();
        System.out.println("子供が変更されました。key=" + key + ", value=" + value);
      }

      @Override
      public void onChildRemoved(DataSnapshot snapshot)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();
        System.out.println("子供が削除されました。key=" + key + ", value=" + value);
      }

      @Override
      public void onChildMoved(DataSnapshot snapshot, String previousChildName)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();
        System.out.println("子供が移動されました。key=" + key + ", value=" + value);
      }

      @Override
      public void onCancelled(DatabaseError error)
      {
        System.out.println("キャンセルされました。");
      }
    };

    reference.child("folder").addChildEventListener(listener);
  }
}

