package test;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import weather_hacks_json.WeatherHacks;

public class Test extends JFrame {
    public static void main(String[] args) {
        JFrame frame = new Test();
        frame.setBounds(100, 100, 300, 250);


    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        final JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(900, 900));


        String json = null;

        try {
            String url = "http://weather.livedoor.com/forecast/webservice/json/v1?city=130010";
            BufferedReader br = null;

            HttpURLConnection con = (HttpURLConnection)new URL(url).openConnection();
            con.setRequestMethod("GET");
            br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            json = br.readLine();
            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        WeatherHacks weatherHacks = new Gson().fromJson(json, WeatherHacks.class);

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

        JLabel place = new JLabel("タイトル");
        place.setText("東京の天気予報");
        place.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 24));
        place.setHorizontalAlignment(JLabel.CENTER);
        JTextArea text= new JTextArea(gson.toJson(weatherHacks),30,15);
        JScrollPane scrollpane = new JScrollPane(text);
        panel.add(scrollpane);

        Container contentPane = frame.getContentPane();
        contentPane.add(place,BorderLayout.NORTH);
        contentPane.add(scrollpane, BorderLayout.SOUTH);

        System.out.println(gson.toJson(weatherHacks));


        }
    }